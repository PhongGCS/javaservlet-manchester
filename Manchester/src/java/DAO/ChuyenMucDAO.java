/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import ConnnectDB.DBConnect;
import Model.ChuyenMuc;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Khac Phong IS
 */
public class ChuyenMucDAO {
    public ArrayList<ChuyenMuc> getListChuyenMuc() {
        Connection cons = DBConnect.getConnecttion();
        String sql = "SELECT * FROM chuyenmuc";
        ArrayList<ChuyenMuc> list = new ArrayList<>();
        try {
            PreparedStatement ps = (PreparedStatement) cons.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ChuyenMuc chuyenmuc = new ChuyenMuc();
                chuyenmuc.setID(rs.getInt("ID"));
                chuyenmuc.setName(rs.getString("TENCHUYENMUC"));
                list.add(chuyenmuc);
            }
            cons.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    
     public ArrayList<ChuyenMuc> getListChuyenMucTheoIDParent(int parent) {
        Connection cons = DBConnect.getConnecttion();
        String sql = "SELECT * FROM chuyenmuc WHERE PARENT ="+parent;
        ArrayList<ChuyenMuc> list = new ArrayList<>();
        try {
            PreparedStatement ps = (PreparedStatement) cons.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                ChuyenMuc chuyenmuc = new ChuyenMuc();
                chuyenmuc.setID(rs.getInt("ID"));
                chuyenmuc.setName(rs.getString("TENCHUYENMUC"));
                chuyenmuc.setID(rs.getInt("PARENT"));
                list.add(chuyenmuc);
            }
            cons.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    
    public static void main(String[] args) {
        ChuyenMucDAO dao = new ChuyenMucDAO();
        for(ChuyenMuc item : dao.getListChuyenMucTheoIDParent(1)){
            System.out.println(item.getName() + "----" + item.getID() );
        }
    }
}
