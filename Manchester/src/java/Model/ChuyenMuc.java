/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Khac Phong IS
 */
public class ChuyenMuc {
    private long ID;
    private String Name;
    private long IDPrent;

    public ChuyenMuc() {
    }

    public ChuyenMuc(long ID, String Name, long IDPrent) {
        this.ID = ID;
        this.Name = Name;
        this.IDPrent = IDPrent;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public long getIDPrent() {
        return IDPrent;
    }

    public void setIDPrent(long IDPrent) {
        this.IDPrent = IDPrent;
    }

    
    
}
