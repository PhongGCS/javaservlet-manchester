package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import Model.ChuyenMuc;
import DAO.ChuyenMucDAO;

public final class header_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Header</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div id=\"header\">\n");
      out.write("\t\t<div class=\"header-top\">\n");
      out.write("\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t<div class=\"pull-left auto-width-left\">\n");
      out.write("\t\t\t\t\t<ul class=\"top-menu menu-beta l-inline\">\n");
      out.write("\t\t\t\t\t\t<li><a href=\"\"><i class=\"fa fa-home\"></i> 90-92 Lê Thị Riêng, Bến Thành, Quận 1</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"\"><i class=\"fa fa-phone\"></i> 0163 296 7751</a></li>\n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"pull-right auto-width-right\">\n");
      out.write("\t\t\t\t\t<ul class=\"top-details menu-beta l-inline\">\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\"><i class=\"fa fa-user\"></i>Tài khoản</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">Đăng kí</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">Đăng nhập</a></li>\n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"clearfix\"></div>\n");
      out.write("\t\t\t</div> <!-- .container -->\n");
      out.write("\t\t</div> <!-- .header-top -->\n");
      out.write("\t\t<div class=\"header-body\">\n");
      out.write("\t\t\t<div class=\"container beta-relative\">\n");
      out.write("\t\t\t\t<div class=\"pull-left\" style=\"width:500px;\">\n");
      out.write("\t\t\t\t\t<a href=\"index.html\" id=\"logo\">\n");
      out.write("\t\t\t\t\t\t<img src=\"assets/dest/images/logo-main.png\" width=\"90px;\" style=\"float:left\">\n");
      out.write("\t\t\t\t\t\t<h1 class=\"textlogo\">MANCHESTER UNITED</h1>\n");
      out.write("\t\t\t\t\t</a>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"pull-right beta-components space-left ov\">\n");
      out.write("\t\t\t\t\t<div class=\"space10\">&nbsp;</div>\n");
      out.write("\t\t\t\t\t<div class=\"beta-comp\">\n");
      out.write("\t\t\t\t\t\t<form role=\"search\" method=\"get\" id=\"searchform\" action=\"/\">\n");
      out.write("\t\t\t\t\t        <input type=\"text\" value=\"\" name=\"s\" id=\"s\" placeholder=\"Nhập từ khóa...\" />\n");
      out.write("\t\t\t\t\t        <button class=\"fa fa-search\" type=\"submit\" id=\"searchsubmit\"></button>\n");
      out.write("\t\t\t\t\t\t</form>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t<div class=\"beta-comp\">\n");
      out.write("\t\t\t\t\t\t<div class=\"cart\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"beta-select\"><i class=\"fa fa-shopping-cart\"></i> Giỏ hàng (Trống) <i class=\"fa fa-chevron-down\"></i></div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"beta-dropdown cart-body\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"cart-item\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"media\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a class=\"pull-left\" href=\"#\"><img src=\"assets/dest/images/products/cart/1.png\" alt=\"\"></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"media-body\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"cart-item-title\">Sample Woman Top</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"cart-item-options\">Size: XS; Colar: Navy</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"cart-item-amount\">1*<span>$49.50</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"cart-item\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"media\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a class=\"pull-left\" href=\"#\"><img src=\"assets/dest/images/products/cart/2.png\" alt=\"\"></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"media-body\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"cart-item-title\">Sample Woman Top</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"cart-item-options\">Size: XS; Colar: Navy</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"cart-item-amount\">1*<span>$49.50</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"cart-item\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"media\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a class=\"pull-left\" href=\"#\"><img src=\"assets/dest/images/products/cart/3.png\" alt=\"\"></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"media-body\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"cart-item-title\">Sample Woman Top</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"cart-item-options\">Size: XS; Colar: Navy</span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t\t<span class=\"cart-item-amount\">1*<span>$49.50</span></span>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"cart-caption\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"cart-total text-right\">Tổng tiền: <span class=\"cart-total-value\">$34.55</span></div>\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"clearfix\"></div>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t\t\t\t<div class=\"center\">\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<div class=\"space10\">&nbsp;</div>\n");
      out.write("\t\t\t\t\t\t\t\t\t\t<a href=\"checkout.html\" class=\"beta-btn primary text-center\">Đặt hàng <i class=\"fa fa-chevron-right\"></i></a>\n");
      out.write("\t\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div> <!-- .cart -->\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t\t<div class=\"clearfix\"></div>\n");
      out.write("\t\t\t</div> <!-- .container -->\n");
      out.write("\t\t</div> <!-- .header-body -->\n");
      out.write("\t\t<div class=\"header-bottom\" style=\"background-color: #0277b8;\">\n");
      out.write("\t\t\t<div class=\"container\">\n");
      out.write("\t\t\t\t<a class=\"visible-xs beta-menu-toggle pull-right\" href=\"#\"><span class='beta-menu-toggle-text'>Menu</span> <i class=\"fa fa-bars\"></i></a>\n");
      out.write("\t\t\t\t<div class=\"visible-xs clearfix\"></div>\n");
      out.write("\t\t\t\t<nav class=\"main-menu\">\n");
      out.write("\t\t\t\t\t<ul class=\"l-inline ov\">\n");
      out.write("\t\t\t\t\t\t<li><a href=\"index.html\">Home</a></li>\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">TIN TỨC TỔNG HỢP</a>\n");
      out.write("\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("                                                            ");
 ChuyenMucDAO dao = new ChuyenMucDAO();
                                                               for(ChuyenMuc item : dao.getListChuyenMucTheoIDParent(1)){ 
      out.write("\n");
      out.write("                                                               <li><a href=\"product_type.html\">");
      out.print(item.getName() );
      out.write("</a></li>\n");
      out.write("                                                                ");
 } 
      out.write("\n");
      out.write("\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">CẦU THỦ</a>\n");
      out.write("\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"product_type.html\">ĐỘI 1</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"product_type.html\">ĐỘI TRẺ</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"product_type.html\">HUYỀN THOẠI</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"product_type.html\">ĐÃ TỪNG THI ĐẤU</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">TỔNG QUAN</a>\n");
      out.write("\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"product_type.html\">LỊCH SỬ</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"product_type.html\">SÂN OLD TRAFFORD</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"product_type.html\">BÀI HÁT TRUYỀN THỐNG</a></li>\n");
      out.write("\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t</li>\n");
      out.write("\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">LỊCH THI ĐẤU</a>\n");
      out.write("\t\t\t\t\t\t\t<ul class=\"sub-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"product_type.html\">LỊCH THI ĐẤU EPL</a></li>\n");
      out.write("\t\t\t\t\t\t\t\t<li><a href=\"product_type.html\">LỊCH THI ĐẤU C1</a></li>\n");
      out.write("\t\t\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t\t\n");
      out.write("\t\t\t\t\t\t<li><a href=\"#\">ÁO ĐẤU</a></li>\n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("\t\t\t\t\t<div class=\"clearfix\"></div>\n");
      out.write("\t\t\t\t</nav>\n");
      out.write("\t\t\t</div> <!-- .container -->\n");
      out.write("\t\t</div> <!-- .header-bottom -->\n");
      out.write("\t</div> <!-- #header -->\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
