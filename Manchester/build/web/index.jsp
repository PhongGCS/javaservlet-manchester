<%-- 
    Document   : index
    Created on : Oct 8, 2017, 1:36:17 PM
    Author     : Khac Phong IS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href='http://fonts.googleapis.com/css?family=Dosis:300,400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/dest/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/dest/vendors/colorbox/example3/colorbox.css">
        <link rel="stylesheet" href="assets/dest/rs-plugin/css/settings.css">
        <link rel="stylesheet" href="assets/dest/rs-plugin/css/responsive.css">
        <link rel="stylesheet" title="style" href="assets/dest/css/style.css">
        <link rel="stylesheet" href="assets/dest/css/animate.css">
        <link rel="stylesheet" title="style" href="assets/dest/css/huong-style.css">
        <style>
            @import url('https://fonts.googleapis.com/css?family=Changa+One|Open+Sans');
            .row-item{
                padding-left:15px;
            }
            .nopadding {
                padding: 0 !important;
            }
            #logo h1{
                font-family:'Changa One',sans-serif;
                margin: 15px 0 15px 10px ;
                font-weight:bold;
                line-height:0.6em;
                color: red;
            }
            #logo a:hover{
                color:#fff;
                text-decoration:none;
            } 
        </style>

    </head>
    <body>
        <jsp:include page="header.jsp"></jsp:include>
        <jsp:include page="slider.jsp"></jsp:include>
            <div class="container" style="margin-top:30px;">
                <div class="row main-left">
                    <div class="col-md-4">
                        <div class="panel panel-default nopadding">
                            <div class="panel-heading" style="background-color:#337AB7; color:white;" >
                                <h2 style="    padding: 10px;"> Tin Tức Cầu Thủ </h2>
                            </div>

                            <div class="panel-body">
                                <!-- item -->
                                <div class="row-item row nopadding">
                                    <div class="first-item">
                                        <div class="col-md-12" style="margin-bottom: 10px; height:300px;">

                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/300x300" alt="" style="margin:auto;">
                                            </a>

                                        </div>

                                        <div class="col-md-12" style="margin-bottom:20px">
                                            <h3>Project Five Lorem ipsum dolor </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.   quo, minima, inventore voluptatum saepe quos nostrum provident .</p>
                                            <a class="btn btn-primary" href="chitiet.html">Xem Chi Tiết <span class="glyphicon glyphicon-chevron-right"></span></a>
                                        </div>
                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>





                                    <div class="break"></div>
                                </div>
                                <!-- end item -->

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color:#337AB7; color:white;" >
                                <h2 style="    padding: 10px;"> Tin Tức Tổng Hợp </h2>
                            </div>

                            <div class="panel-body">
                                <!-- item -->
                                <div class="row-item row nopadding">
                                    <div class="first-item">
                                        <div class="col-md-12" style="margin-bottom: 10px; height:300px;">

                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/300x300" alt="" style="margin:auto;">
                                            </a>

                                        </div>

                                        <div class="col-md-12" style="margin-bottom:20px">
                                            <h3>Project Five Lorem ipsum dolor </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.   quo, minima, inventore voluptatum saepe quos nostrum provident .</p>
                                            <a class="btn btn-primary" href="chitiet.html">Xem Chi Tiết <span class="glyphicon glyphicon-chevron-right"></span></a>
                                        </div>
                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>





                                    <div class="break"></div>
                                </div>
                                <!-- end item -->

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color:#337AB7; color:white;" >
                                <h2 style="    padding: 10px;"> Tin Tức</h2>
                            </div>

                            <div class="panel-body">
                                <!-- item -->
                                <div class="row-item row nopadding">
                                    <div class="first-item">
                                        <div class="col-md-12" style="margin-bottom: 10px; height:300px;">

                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/300x300" alt="" style="margin:auto;">
                                            </a>

                                        </div>

                                        <div class="col-md-12" style="margin-bottom:20px">
                                            <h3>Project Five Lorem ipsum dolor </h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit.   quo, minima, inventore voluptatum saepe quos nostrum provident .</p>
                                            <a class="btn btn-primary" href="chitiet.html">Xem Chi Tiết <span class="glyphicon glyphicon-chevron-right"></span></a>
                                        </div>
                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>

                                    <div class="col-md-12 nopadding" style="margin-bottom: 10px">
                                        <div class="col-md-4">
                                            <a href="chitiet.html">
                                                <img class="img-responsive" src="http://via.placeholder.com/350x350" alt="" style="width: 100px;height: 100px;">
                                            </a>
                                        </div>

                                        <div class="col-md-8">
                                            <b>Project Five Lorem ipsum dolor </b>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, Lorem ipsum dolor sit amet</p>
                                        </div>

                                    </div>





                                    <div class="break"></div>
                                </div>
                                <!-- end item -->

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row lich-thi-dau">
                    <div class="col-md-8">
                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color:#337AB7; color:white;" >
                                <h2 style="padding: 10px;"> LỊCH THI ĐẤU</h2>
                            </div>
                        </div>
                        <div class="panel-body nopadding">
                            <table class="table table-bordered">
                                <thead>
                                    <tr  style="background-color: darkgray;">
                                        <th colspan="3"> <center>23/9/2017</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>4 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>2 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>0 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <thead>
                                    <tr style="background-color: darkgray;">
                                        <th colspan="3"> <center>23/9/2017</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>4 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>2 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>0 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <thead>
                                    <tr  style="background-color: darkgray;">
                                        <th colspan="3"> <center>23/9/2017</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>4 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>2 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>0 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <thead>
                                    <tr  style="background-color: darkgray;">
                                        <th colspan="3"> <center>23/9/2017</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>4 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>2 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>0 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered">
                                <thead>
                                    <tr  style="background-color: darkgray;">
                                        <th colspan="3"> <center>23/9/2017</center></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>4 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>2 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                    <tr>
                                        <td>West Ham United</td>
                                        <td>0 : 1</td>
                                        <td>Tottenham Hotspur</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="col-md-12 nopadding">
                            <a href="chitiet.html">
                                <img class="img-responsive" src="http://via.placeholder.com/500x500" alt="">
                            </a>
                        </div>
                        <div class="col-md-12 nopadding">					
                            <a href="chitiet.html">
                                <img class="img-responsive" src="http://via.placeholder.com/500x500" alt="">
                            </a>
                        </div>
                    </div>

                </div>

                <div id="content" class="space-top-none">
                    <div class="main-content">
                        <div class="space60">&nbsp;</div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="beta-products-list">
                                    <h4>New Products</h4>
                                    <div class="beta-products-details">
                                        <p class="pull-left">438 styles found</p>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/1.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span>$34.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>

                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/2.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span class="flash-del">$34.55</span>
                                                        <span class="flash-sale">$33.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/3.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span>$34.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/3.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span>$34.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- .beta-products-list -->

                                <div class="space50">&nbsp;</div>

                                <div class="beta-products-list">
                                    <h4>Top Products</h4>
                                    <div class="beta-products-details">
                                        <p class="pull-left">438 styles found</p>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/1.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span>$34.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>

                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/2.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span class="flash-del">$34.55</span>
                                                        <span class="flash-sale">$33.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/3.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span>$34.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/3.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span>$34.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="space40">&nbsp;</div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/1.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span>$34.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="ribbon-wrapper"><div class="ribbon sale">Sale</div></div>

                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/2.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span class="flash-del">$34.55</span>
                                                        <span class="flash-sale">$33.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/3.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span>$34.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single-item">
                                                <div class="single-item-header">
                                                    <a href="product.html"><img src="assets/dest/images/products/3.jpg" alt=""></a>
                                                </div>
                                                <div class="single-item-body">
                                                    <p class="single-item-title">Sample Woman Top</p>
                                                    <p class="single-item-price">
                                                        <span>$34.55</span>
                                                    </p>
                                                </div>
                                                <div class="single-item-caption">
                                                    <a class="add-to-cart pull-left" href="shopping_cart.html"><i class="fa fa-shopping-cart"></i></a>
                                                    <a class="beta-btn primary" href="product.html">Details <i class="fa fa-chevron-right"></i></a>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- .beta-products-list -->
                            </div>
                        </div> <!-- end section with sidebar and main content -->


                    </div> <!-- .main-content -->
                </div> <!-- #content -->
            </div> <!-- .container -->

        <jsp:include page="footer.jsp"></jsp:include>

        <!-- include js files -->
        <script src="assets/dest/js/jquery.js"></script>
        <script src="assets/dest/vendors/jqueryui/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
        <script src="assets/dest/vendors/bxslider/jquery.bxslider.min.js"></script>
        <script src="assets/dest/vendors/colorbox/jquery.colorbox-min.js"></script>
        <script src="assets/dest/vendors/animo/Animo.js"></script>
        <script src="assets/dest/vendors/dug/dug.js"></script>
        <script src="assets/dest/js/scripts.min.js"></script>
        <script src="assets/dest/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
        <script src="assets/dest/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script src="assets/dest/js/waypoints.min.js"></script>
        <script src="assets/dest/js/wow.min.js"></script>
        <!--customjs-->
        <script src="assets/dest/js/custom2.js"></script>
        <script>
            $(document).ready(function ($) {
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 150) {
                        $(".header-bottom").addClass('fixNav')
                    } else {
                        $(".header-bottom").removeClass('fixNav')
                    }
                }
                )
            })
        </script>
    </body>
</html>
